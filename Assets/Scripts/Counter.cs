﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour
{
    // public Text pointText;
    public int num = 0;
    void Start(){
        num = PlayerPrefs.GetInt ("LastNum");
        // pointText.text = "" + num;
    }
 
    public int IncreaseNum(){
        print("++");
        num = PlayerPrefs.GetInt("LastNum") + 1;
        PlayerPrefs.SetInt ("LastNum", num);
        return num;
    }
    public int DecreaseNum(){
        num--;
        PlayerPrefs.SetInt ("LastNum", num);
        return num;
    }
    public void Reset(){
        num = 0;
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetInt ("LastNum", num);
        // pointText.text = "" + num;
    }

    public int GetSavedPoint(){
        PlayerPrefs.SetInt ("LastNum", num);
        return num;
    }

    public void saveButtonState(string buttonName){
        PlayerPrefs.SetInt (buttonName, 1);
    }

    public int loadButtonState(string buttonName){
        int buttonState = PlayerPrefs.GetInt (buttonName);
        return buttonState;
    }
}
