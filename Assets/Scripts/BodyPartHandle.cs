﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Counter))]
public class BodyPartHandle : MonoBehaviour
{
    public string stickerName1;
    public string stickerName2;
    public string stickerName3;
    private string componentName;
    public GameObject panel1;
    public GameObject panel2;
    public GameObject panel3;
    public Text pointText;
    private Counter counterHandler;
    private int buttonState = 0;
    public GameObject findStiker;
    public Material strikerDoneMaterial;
    public Material strikerNormalMaterial;

    // Start is called before the first frame update
    void Start()
    {
        GameObject gameObject = new GameObject();
        counterHandler = gameObject.AddComponent<Counter>();
        pointText.text = "" + PlayerPrefs.GetInt ("LastNum");

        checkButtonState(findStiker, stickerName1);
        checkButtonState(findStiker, stickerName2);
        checkButtonState(findStiker, stickerName3);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit Hit;
            if (Physics.Raycast(ray, out Hit))
            {
                componentName = Hit.transform.name;
                if (componentName == stickerName1) {
                    showPanel(panel1, stickerName1);
                } 
                if (componentName == stickerName2) {
                    showPanel(panel2, stickerName2);
                }
                if (componentName == stickerName3) {
                    showPanel(panel3, stickerName3);
                }
            }
        }
    }
    public void showPanel(GameObject panel, string stickerName)
    {
        int loadState = counterHandler.loadButtonState(stickerName);
        buttonState = loadState;
        print("Panel " + stickerName);
        panel.SetActive(true);
        if(buttonState == 0){
            pointText.text = "" + counterHandler.IncreaseNum();
            counterHandler.saveButtonState(stickerName);
            changeButtonState(findStiker, stickerName);
        }
    }

    public void changeButtonState(GameObject stiker, string stickerName){
        stiker = GameObject.Find(stickerName);
        stiker.GetComponent<Renderer>().material = strikerDoneMaterial;
    }

    public void checkButtonState(GameObject stiker, string stickerName){
        stiker = GameObject.Find(stickerName);
        int loadState = counterHandler.loadButtonState(stickerName);
        buttonState = loadState;
        if(buttonState == 1){
            stiker.GetComponent<Renderer>().material = strikerDoneMaterial;
        } else stiker.GetComponent<Renderer>().material = strikerNormalMaterial;
    }
}
